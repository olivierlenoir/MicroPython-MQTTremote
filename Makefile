# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-07-14 11:27:08
# Project: MicroPythonMQTTremote

TEXFILE = MicroPythonMQTTremote

all: make_gv make_doc clean


.SILENT: clean


make_gv:
	convert img/MQTT.svg -resize 100x100 img/MQTT.png
	dot -Tpdf gv/MQTTremote.gv > gv/MQTTremote.pdf
	dot -Tpng gv/MQTTremote.gv > gv/MQTTremote.png


make_doc:
	pdflatex ${TEXFILE}.tex
	pdflatex ${TEXFILE}.tex
	pdflatex ${TEXFILE}.tex


clean:
	rm -f ${TEXFILE}.aux
	rm -f ${TEXFILE}.log
	rm -f ${TEXFILE}.out
	rm -f ${TEXFILE}.nav
	rm -f ${TEXFILE}.toc
