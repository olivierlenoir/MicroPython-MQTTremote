# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-07-14 12:14:07
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.16
# Project: MicroPython MQTT remote
# Description:

from ujson import load, loads, dumps
from machine import unique_id, Timer
from ubinascii import hexlify
import network
from utime import sleep_ms
from umqtt.simple import MQTTClient


class MQTTremote(object):

    def __init__(self, config='config.json'):
        with open(config) as conf:
            for k, v in load(conf).items():
                setattr(self, k, v)
        self.msg_queue = []
        self.connect()

    def connect(self):
        """Connect WLAN and MQTT"""
        # wlan connect
        self.wlan = network.WLAN(network.STA_IF)
        self.wlan.active(True)
        if not self.wlan.isconnected():
            self.wlan.connect(**self.WLAN)
            while not self.wlan.isconnected():
                sleep_ms(200)
        # mqtt connect
        self.mqtt = MQTTClient(**self.MQTT)
        if self.MRT['rmt_id'] is None:
            # emitter
            pass
        else:
            # receiver
            self.mqtt.set_callback(self._sub_cb)
            self.mqtt.connect()
            self.mqtt.subscribe(b'MQTTrmt/{rmt_id}/json}'.format(**self.RMT))
        # connected
        self.connected = True

    def disconnect(self):
        """Disconnect MQTT and WLAN"""
        # mqtt disconnect
        self.mqtt.disconnect()
        # wlan disconnect
        if self.wlan.isconnected():
            self.wlan.disconnect()
            while self.wlan.isconnected():
                sleep_ms(200)
        # disconnected
        self.connected = False

    @staticmethod
    def _unique_id(byte=True):
        """Return unique_id of the device, byte or utf-8 format"""
        u_id = hexlify(unique_id())
        return u_id if byte else u_id.decode('utf-8')

    def _sub_cb(self, topic, msg):
        """MQTT subscribe callback"""
        self.msg_queue.append((topic, loads(msg)))
